Audirvana rev. 0.9.5 debug information:

Hog Mode is off
Devices found : 17

List of devices:
Device #0: ID 0xd3 Apowersoft_AudioDevice	UID:ApowersoftEngine:0
Device #1: ID 0xfd Built-in Output	UID:AppleHDAEngineOutput:1B,0,1,1:0
Device #2: ID 0xc6 Boom2Device	UID:Boom2Engine:0
Device #3: ID 0xb9 BoomDevice	UID:BoomEngine:0
Device #4: ID 0x107 iLoveAudioDevice	UID:HlbAudioEngine:0
Device #5: ID 0xe9 Leawo Audio Device	UID:LeawoAudioEngine:0
Device #6: ID 0xde RecordSoundDriver	UID:RecordSoundDriverEngine:0
Device #7: ID 0xae Soundflower (2ch)	UID:SoundflowerEngine:0
Device #8: ID 0x27 Soundflower (64ch)	UID:SoundflowerEngine:1
Device #9: ID 0xf4 Instant On Sound Effects	UID:com.rogueamoeba.InstantOn:SoundEffects
Device #10: ID 0x124 RecordSound_AggreDevice	UID:~:Aggregate:0
Device #11: ID 0x13c LewaoAudio_AggerDevice	UID:~:Aggregate:1
Device #12: ID 0x10c ApowersoftAudio_AggreDevice	UID:~:Aggregate:2
Device #13: ID 0x154 iLoveAudio_AggerDevice	UID:~:Aggregate:3
Device #14: ID 0x16c System Built-in Output	UID:~:StackedOutput:0
Device #15: ID 0x170 System Built-in Output	UID:~:StackedOutput:1
Device #16: ID 0x174 System Buiil-in Output	UID:~:StackedOutput:2

Preferred device: Apowersoft_AudioDevice	UID:ApowersoftEngine:0

Selected device:
ID 0xd3 Apowersoft_AudioDevice	UID:ApowersoftEngine:0
6 available sample rates up to 192000.0Hz
44100.0
48000.0
88200.0
96000.0
176400.0
192000.0

Audio buffer frame size : 58 to 4096 frames
Current I/O buffer frame size : 512
Physical (analog) volume control: Yes
Virtual (digital) volume control: Yes
Preferred stereo channels L:1 R:2
Simple stereo device: yes
Channel mapping: L:Stream 0 channel 0 R:Stream 0 channel 1

1 output streams:
Stream ID 0xd4 2 channels starting at 1
6 virtual formats:
Mixable linear PCM Interleaved 32bits little endian  Float @192.0kHz
Mixable linear PCM Interleaved 32bits little endian  Float @176.4kHz
Mixable linear PCM Interleaved 32bits little endian  Float @96.0kHz
Mixable linear PCM Interleaved 32bits little endian  Float @88.2kHz
Mixable linear PCM Interleaved 32bits little endian  Float @48.0kHz
Mixable linear PCM Interleaved 32bits little endian  Float @44.1kHz

6 physical formats
Mixable linear PCM Interleaved 32bits big endian Signed Integer @192.0kHz
Mixable linear PCM Interleaved 32bits big endian Signed Integer @176.4kHz
Mixable linear PCM Interleaved 32bits big endian Signed Integer @96.0kHz
Mixable linear PCM Interleaved 32bits big endian Signed Integer @88.2kHz
Mixable linear PCM Interleaved 32bits big endian Signed Integer @48.0kHz
Mixable linear PCM Interleaved 32bits big endian Signed Integer @44.1kHz