Audirvana rev. 0.9.5 debug information:

Hog Mode is off
Devices found : 7

List of devices:
Device #0: ID 0x30 Built-in Output	UID:AppleHDAEngineOutput:1B,0,1,1:0
Device #1: ID 0xe0 Boom2Device	UID:Boom2Engine:0
Device #2: ID 0xd3 BoomDevice	UID:BoomEngine:0
Device #3: ID 0xc8 Soundflower (2ch)	UID:SoundflowerEngine:0
Device #4: ID 0x41 Soundflower (64ch)	UID:SoundflowerEngine:1
Device #5: ID 0x26 KORG USB Audio Device Driver	UID:com_korg_usb_dac_UsbpalAudioEngine:0
Device #6: ID 0xed System Built-in Output	UID:~:StackedOutput:1

Preferred device: System Built-in Output	UID:~:StackedOutput:1

Selected device:
ID 0xed System Built-in Output	UID:~:StackedOutput:1
6 available sample rates up to 192000.0Hz
44100.0
48000.0
88200.0
96000.0
176400.0
192000.0

Audio buffer frame size : 58 to 4096 frames
Current I/O buffer frame size : 512
Physical (analog) volume control: No
Virtual (digital) volume control: No
Preferred stereo channels L:1 R:2
Simple stereo device: yes
Channel mapping: L:Stream 0 channel 0 R:Stream 0 channel 1

1 output streams:
Stream ID 0xee 2 channels starting at 1
6 virtual formats:
Mixable linear PCM Interleaved 32bits little endian  Float @192.0kHz
Mixable linear PCM Interleaved 32bits little endian  Float @176.4kHz
Mixable linear PCM Interleaved 32bits little endian  Float @96.0kHz
Mixable linear PCM Interleaved 32bits little endian  Float @88.2kHz
Mixable linear PCM Interleaved 32bits little endian  Float @48.0kHz
Mixable linear PCM Interleaved 32bits little endian  Float @44.1kHz

6 physical formats
Mixable linear PCM Interleaved 32bits little endian  Float @192.0kHz
Mixable linear PCM Interleaved 32bits little endian  Float @176.4kHz
Mixable linear PCM Interleaved 32bits little endian  Float @96.0kHz
Mixable linear PCM Interleaved 32bits little endian  Float @88.2kHz
Mixable linear PCM Interleaved 32bits little endian  Float @48.0kHz
Mixable linear PCM Interleaved 32bits little endian  Float @44.1kHz